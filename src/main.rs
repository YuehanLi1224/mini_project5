use aws_sdk_dynamodb::model::AttributeValue;
use aws_sdk_dynamodb::Client;
use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use rand::Rng;
use serde::{Deserialize, Serialize};
use tracing_subscriber::filter::{EnvFilter, LevelFilter};

// Define a struct to hold the dice rolling result
#[derive(Debug, Serialize, Deserialize)]
struct DiceRoll {
    id: String,
    value: u32,
}

/// This is a made-up example. Requests come into the runtime as unicode
/// strings in json format, which can map to any structure that implements `serde::Deserialize`
/// The runtime pays no attention to the contents of the request payload.
#[derive(Deserialize)]
struct Request {
    command: String,
}

/// This is a made-up example of what a response structure may look like.
/// There is no restriction on what it can be. The runtime requires responses
/// to be serialized into json. The runtime pays no attention
/// to the contents of the response payload.
#[derive(Serialize)]
struct Response {
    req_id: String,
    msg: String,
}

/// This is the main body for the function.
/// Write your code inside it.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples
/// - https://github.com/aws-samples/serverless-rust-demo/
async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let result = rand::thread_rng().gen_range(1..=6);

    // Generate a unique ID for the dice roll
    let id = event.context.request_id;

    // Create a new DiceRoll instance
    let dice_roll = DiceRoll {
        id: id.clone(),
        value: result,
    };

    // Initialize the DynamoDB client
    let config = aws_config::load_from_env().await;
    let client = Client::new(&config);

    // Prepare the item for putting into DynamoDB
    let id_attr = AttributeValue::S(dice_roll.id.clone());
    let value_attr = AttributeValue::N(dice_roll.value.to_string());

    // Define the table name
    let table_name = "dice_rolls"; 

    // Put the item into the table
    let _resp = client
        .put_item()
        .table_name(table_name)
        .item("id", id_attr)
        .item("value", value_attr)
        .send()
        .await?;

    let resp = Response {
        req_id: id,
        msg: format!("You rolled a {}", result),
    };

    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
